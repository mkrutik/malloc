# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/16 10:00:24 by mkrutik           #+#    #+#              #
#    Updated: 2018/12/16 10:01:25 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

NAME = libft_malloc_$(HOSTTYPE).so

RED 	= \033[0;31m
GREEN 	= \033[1;32m
ORANGE 	= \033[0;33m
GRAY 	= \033[1;30m
DEF 	= \033[0m

SRC = 	src/calloc.c \
		src/free.c \
		src/libft_helpers.c \
		src/malloc.c \
		src/malloc_helpers_1.c \
		src/malloc_helpers_2.c \
		src/print_info.c \
		src/realloc.c


OBJ = $(SRC:.c=.o)

HEADERS = -I./include

COMPILER = gcc

FLAGS = -g -fPIC -Wall -Wextra -Werror -lpthread #-Wl,-z, defs -Wl,--version-script=./libmalloc.version

all: $(OBJ)
	@ $(COMPILER) $(FLAGS) -shared $(OBJ) -o $(NAME)
	@ln -sf $(NAME) libft_malloc.so
	@ echo "${GREEN}Compile binary file was successful${DEF}"

%.o : %.c
	@ $(COMPILER) -g -fPIC $(HEADERS) -o $@ -c $<
	@ echo "${GRAY}Compile object files${DEF}"

clean:
	@ rm -f $(OBJ)
	@ echo "${ORANGE}Object files was successfuly remowed${DEF}"

fclean: clean
	@ rm -f $(NAME)
	@ rm -f libft_malloc.so
	@ echo "${RED}Binary file was successfuly remowed${DEF}"

re: fclean all