/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hidden.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:11:23 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 10:00:56 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HIDDEN_H
# define HIDDEN_H

# include <pthread.h>
# include <unistd.h>
# include <sys/mman.h>
# include <sys/time.h>
# include <sys/resource.h>

# define TRUE 1
# define FALSE 0

# define MIN_ALLOCATION 100

# define TINY_MAX_SIZE  (getpagesize() / 8)
# define SMALL_MAX_SIZE (getpagesize())

# define TINY_BLOCK_MIN_SIZE (TINY_MAX_SIZE * MIN_ALLOCATION)
# define SMALL_BLOCK_MIN_SIZE (SMALL_MAX_SIZE * MIN_ALLOCATION)
# define LARGE_BLOCK_MIN_SIZE (SMALL_MAX_SIZE * 2) * MIN_ALLOCATION

typedef enum		e_type {
	TINY = 0,
	SMALL = 1,
	LARGE = 2
}					t_type;

typedef struct		s_block {
	unsigned		size;
	unsigned		used;
	void			*ptr;
	struct s_block	*next;
}					t_block;

typedef struct		s_all_info {
	size_t			page_size;
	size_t			max_range[2];
	size_t			m_list_size[3];
	size_t			max_allowed_size;

	t_block			*b_lists[3];
	size_t			c_l_sizes[3];
	size_t			f_l_size[3];
}					t_all_info;

/*
** Global variable
*/
pthread_mutex_t		g_mu;
t_all_info			g_info;

/*
** Mallocs helper functions
*/
void				init(void);
t_type				get_type(size_t size);
t_block				*get_last(t_type type);
t_block				*get_block_by_adress(void *ptr);
t_block				*find_empty_block(t_type type, size_t block_size);
void				*add_block(t_type type, size_t size_needed, char cleaning);
t_block				*alloc_block(t_type type, size_t size_needed);
t_block				*defragmentation(t_type type, size_t size, t_block *res);
void				split_block(t_type type, t_block *block, size_t size_need);
void				free_block(t_type type, t_block *block);
/*
** Libft functions
*/
void				ft_bzero(void *src, size_t size);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				ft_putstr(const char *str);
void				itoa_base_print(unsigned value, int base);
#endif
