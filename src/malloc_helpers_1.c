/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_helpers_1.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:43:56 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 09:59:17 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/malloc.h"
#include "../include/hidden.h"

void	split_block(t_type type, t_block *block, size_t size_needed)
{
	t_block	*tmp;

	if ((long)(block->size - size_needed) > (long)sizeof(t_block))
	{
		tmp = (t_block*)((char*)block + size_needed);
		tmp->next = block->next;
		tmp->used = FALSE;
		tmp->size = block->size - size_needed;
		tmp->ptr = (void*)((char*)tmp + sizeof(t_block));
		block->size = size_needed;
		block->next = tmp;
		g_info.f_l_size[type] += tmp->size;
	}
}

t_block	*find_empty_block(t_type type, size_t size_needed)
{
	t_block	*res;

	res = g_info.b_lists[type];
	while (res != NULL)
	{
		if (res->used == FALSE && res->size >= size_needed)
		{
			res->used = TRUE;
			g_info.f_l_size[type] -= res->size;
			if ((res->size - size_needed) > sizeof(t_block))
				split_block(type, res, size_needed);
			return (res);
		}
		res = res->next;
	}
	return (res);
}

t_block	*alloc_block(t_type type, size_t size_needed)
{
	size_t		alloc_size;
	t_block		*tmp;
	void		*mem_block;

	alloc_size = (size_needed + (size_needed % g_info.page_size)) * 2;
	mem_block = mmap(0, alloc_size,
		PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
	if (!mem_block)
		return (NULL);
	tmp = (t_block *)mem_block;
	tmp->used = FALSE;
	tmp->ptr = (t_block*)((char*)tmp + sizeof(t_block));
	tmp->next = NULL;
	tmp->size = alloc_size;
	if ((long)(alloc_size - size_needed) > (long)sizeof(t_block))
		split_block(type, tmp, size_needed);
	g_info.c_l_sizes[type] += alloc_size;
	g_info.f_l_size[type] += tmp->size;
	return (tmp);
}

t_block	*defragmentation(t_type type, size_t size_needed, t_block *res)
{
	t_block	*head;

	head = g_info.b_lists[type];
	while (head->next != NULL)
	{
		if (head->used == FALSE && head->next->used == FALSE)
		{
			head->size += head->next->size;
			head->next = head->next->next;
			if (res == NULL && head->size >= size_needed)
			{
				res = head;
				res->used = TRUE;
				res->ptr = (void*)((char*)res + sizeof(t_block));
				g_info.f_l_size[type] -= res->size;
				if ((res->size - size_needed) > sizeof(t_block))
					split_block(type, res, size_needed);
			}
		}
		else
			head = head->next;
	}
	return (res);
}

void	*add_block(t_type type, size_t size_needed, char cleaning)
{
	t_block		*res_block;
	t_block		*head;

	res_block = find_empty_block(type, size_needed);
	if (res_block == NULL && g_info.f_l_size[type] >= size_needed)
		res_block = defragmentation(type, size_needed, NULL);
	if (res_block == NULL)
	{
		res_block = alloc_block(type, size_needed);
		if (res_block != NULL)
		{
			res_block->used = TRUE;
			g_info.f_l_size[type] -= res_block->size;
			head = get_last(type);
			head->next = res_block;
		}
	}
	if (res_block != NULL)
	{
		if (cleaning == TRUE)
			ft_bzero(res_block->ptr, res_block->size - sizeof(t_block));
		return (res_block->ptr);
	}
	return (NULL);
}
