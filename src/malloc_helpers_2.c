/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_helpers_2.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:36:21 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 10:01:52 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/malloc.h"
#include "../include/hidden.h"

void	init(void)
{
	struct rlimit	lim;
	int				code;

	g_info.page_size = getpagesize();
	g_info.m_list_size[TINY] = TINY_BLOCK_MIN_SIZE;
	g_info.m_list_size[SMALL] = SMALL_BLOCK_MIN_SIZE;
	g_info.m_list_size[LARGE] = LARGE_BLOCK_MIN_SIZE;
	g_info.c_l_sizes[TINY] = 0;
	g_info.c_l_sizes[SMALL] = 0;
	g_info.c_l_sizes[LARGE] = 0;
	g_info.f_l_size[TINY] = 0;
	g_info.f_l_size[SMALL] = 0;
	g_info.f_l_size[LARGE] = 0;
	g_info.max_range[TINY] = TINY_MAX_SIZE;
	g_info.max_range[SMALL] = SMALL_MAX_SIZE;
	g_info.max_allowed_size = 0;
	code = getrlimit(RLIMIT_DATA, &lim);
	if (code != -1 && lim.rlim_max > 0)
		g_info.max_allowed_size = lim.rlim_max;
	g_info.b_lists[TINY] = alloc_block(TINY, g_info.m_list_size[TINY]);
	g_info.b_lists[SMALL] = alloc_block(SMALL, g_info.m_list_size[SMALL]);
	g_info.b_lists[LARGE] = alloc_block(LARGE, g_info.m_list_size[LARGE]);
}

t_block	*get_block_by_adress(void *ptr)
{
	t_block		*res;
	unsigned	i;

	i = 0;
	while (i < 3)
	{
		res = g_info.b_lists[i];
		while (res)
		{
			if (res->ptr == ptr)
				return (res);
			res = res->next;
		}
		++i;
	}
	return (NULL);
}

void	return_mem_pages(t_type type, t_block *b)
{
	int			res;
	size_t		size;
	t_block		*rem;

	size = b->size;
	b->size = (b->size - sizeof(t_block)) % g_info.page_size + sizeof(t_block);
	if ((g_info.c_l_sizes[type] - (size - b->size)) < g_info.m_list_size[type])
		b->size += g_info.m_list_size[type] -
			(g_info.c_l_sizes[type] - (size - b->size));
	rem = (t_block*)((char*)b + b->size);
	b->next = rem;
	rem->size = size - b->size;
	size = rem->size;
	rem->used = FALSE;
	rem->next = NULL;
	res = munmap((void*)rem, rem->size);
	if (res == 0)
	{
		g_info.c_l_sizes[type] -= size;
		g_info.f_l_size[type] -= size;
		b->next = NULL;
	}
}

void	free_block(t_type type, t_block *block)
{
	block->used = FALSE;
	g_info.f_l_size[type] += block->size;
	if (g_info.c_l_sizes[type] > g_info.m_list_size[type] &&
		g_info.f_l_size[type] > (g_info.c_l_sizes[type] / 2))
	{
		defragmentation(type, g_info.c_l_sizes[type] * 2, NULL);
		block = get_last(type);
		if (block->used == FALSE && ((block->size / g_info.page_size) > 1 ||
			((block->size / g_info.page_size) == 1 &&
				(block->size % g_info.page_size) >= sizeof(t_block))))
			return_mem_pages(type, block);
	}
}

t_type	get_type(size_t size)
{
	t_type	type;

	if (size <= g_info.max_range[TINY])
		type = TINY;
	else if (size <= g_info.max_range[SMALL])
		type = SMALL;
	else
		type = LARGE;
	return (type);
}
