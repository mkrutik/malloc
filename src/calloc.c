/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:23:33 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 09:24:46 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/malloc.h"
#include "../include/hidden.h"

void	*calloc(size_t number, size_t size)
{
	void	*res;
	size_t	block_size;
	t_type	type;

	res = NULL;
	pthread_mutex_lock(&g_mu);
	if (!g_info.b_lists[TINY] || !g_info.b_lists[SMALL] ||
		!g_info.b_lists[LARGE])
		init();
	if (number == 0 || size == 0)
	{
		pthread_mutex_unlock(&g_mu);
		return (NULL);
	}
	block_size = (number * size) + sizeof(t_block);
	type = get_type(block_size);
	res = add_block(type, block_size, TRUE);
	pthread_mutex_unlock(&g_mu);
	return (res);
}
