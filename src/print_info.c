/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_info.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:39:23 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 09:46:38 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/hidden.h"

size_t	print_range(t_type type)
{
	t_block	*p;
	size_t	res;

	p = g_info.b_lists[type];
	res = 0;
	while (p)
	{
		if (p->used == TRUE)
		{
			itoa_base_print((size_t)(char*)(p + sizeof(t_block)), 16);
			ft_putstr(" - ");
			itoa_base_print((size_t)((char*)p + p->size - 1), 16);
			ft_putstr(" : ");
			itoa_base_print(p->size - sizeof(t_block), 10);
			ft_putstr(" bytes\n");
			res += p->size - sizeof(t_block);
		}
		p = p->next;
	}
	ft_putstr("\n");
	return (res);
}

void	show_alloc_mem(void)
{
	size_t	total;

	total = 0;
	ft_putstr("TINY : ");
	itoa_base_print((size_t)(g_info.b_lists[TINY]), 16);
	ft_putstr("\n");
	total += print_range(TINY);
	ft_putstr("SMALL : ");
	itoa_base_print((size_t)(g_info.b_lists[SMALL]), 16);
	ft_putstr("\n");
	total += print_range(SMALL);
	ft_putstr("LARGE : ");
	itoa_base_print((size_t)(g_info.b_lists[LARGE]), 16);
	ft_putstr("\n");
	total += print_range(LARGE);
	ft_putstr("TOTAL : ");
	itoa_base_print(total, 10);
	ft_putstr(" bytes\n");
}

t_block	*get_last(t_type type)
{
	t_block	*res;

	res = g_info.b_lists[type];
	while (res && res->next)
		res = res->next;
	return (res);
}
