/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:26:52 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 10:02:16 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/malloc.h"
#include "../include/hidden.h"

void	concat_with_next(t_type type, t_block *curr_block)
{
	while (curr_block && curr_block->next && curr_block->next->used == FALSE)
	{
		g_info.f_l_size[type] -= curr_block->next->size;
		curr_block->size += curr_block->next->size;
		curr_block->next = curr_block->next->next;
	}
}

t_block	*resize_cont(t_type type, t_block *res, size_t n_size)
{
	t_block *head;

	if (g_info.f_l_size[type] >= n_size)
		res = defragmentation(type, n_size, NULL);
	if (res == NULL)
	{
		type = get_type(n_size);
		res = alloc_block(type, n_size);
		if (res != NULL)
		{
			g_info.f_l_size[type] -= res->size;
			res->used = TRUE;
			head = g_info.b_lists[type];
			while (head && head->next)
				head = head->next;
			if (head)
				head->next = res;
		}
	}
	return (res);
}

t_block	*resize_block(t_type type, t_block *c_block, size_t n_size)
{
	t_block		*res;
	t_block		*head;
	t_type		new_type;

	concat_with_next(type, c_block);
	if (c_block->size >= n_size)
	{
		split_block(type, c_block, n_size);
		return (c_block);
	}
	res = find_empty_block(type, n_size);
	if (res == NULL)
		res = resize_cont(type, res, n_size);
	c_block->used = FALSE;
	g_info.f_l_size[type] += c_block->size;
	n_size = (n_size < c_block->size) ? (n_size - sizeof(t_block)) :
		(c_block->size - sizeof(t_block));
	if (res != NULL)
		ft_memcpy(res->ptr, c_block->ptr, n_size);
	return (res);
}

void	*concat_or_create_block(t_block *curr_block, size_t size)
{
	t_type	type;

	type = get_type(curr_block->size);
	if (curr_block->size > size)
	{
		split_block(type, curr_block, size);
		return (curr_block);
	}
	curr_block = resize_block(type, curr_block, size);
	if (curr_block)
		return (curr_block->ptr);
	return (NULL);
}

void	*realloc(void *ptr, size_t size)
{
	void		*res;
	t_block		*block;

	pthread_mutex_lock(&g_mu);
	if (!g_info.b_lists[TINY] || !g_info.b_lists[SMALL] ||
		!g_info.b_lists[LARGE])
		init();
	if (ptr != NULL && ((block = get_block_by_adress(ptr)) == NULL ||
		block->size == size))
	{
		pthread_mutex_unlock(&g_mu);
		return (NULL);
	}
	res = NULL;
	if (ptr == NULL && size == 0)
		res = NULL;
	else if (ptr != NULL && size != 0)
		res = concat_or_create_block(block, size + sizeof(t_block));
	else if (ptr == NULL && size != 0)
		res = malloc(size);
	else if (ptr != NULL && size == 0)
		free(ptr);
	pthread_mutex_unlock(&g_mu);
	return (res);
}
