/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:26:32 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 09:26:34 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/malloc.h"
#include "../include/hidden.h"

void	*malloc(size_t size)
{
	void	*res;
	size_t	block_size;
	t_type	type;

	res = NULL;
	pthread_mutex_lock(&g_mu);
	if (!g_info.b_lists[TINY] || !g_info.b_lists[SMALL] ||
		!g_info.b_lists[LARGE])
		init();
	if (size == 0)
	{
		pthread_mutex_unlock(&g_mu);
		return (NULL);
	}
	block_size = size + sizeof(t_block);
	type = get_type(block_size);
	res = add_block(type, block_size, FALSE);
	pthread_mutex_unlock(&g_mu);
	return (res);
}
