/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_helpers.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:42:20 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 09:43:41 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/hidden.h"
#include "../include/malloc.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char		*pdst;
	unsigned char		*psrc;
	size_t				index;

	if (!dst || !src || n == 0)
		return (dst);
	pdst = (unsigned char *)dst;
	psrc = (unsigned char *)src;
	index = 0;
	while (index++ < n)
	{
		*pdst++ = *psrc++;
	}
	return (dst);
}

void	ft_bzero(void *src, size_t size)
{
	unsigned char	*tmp;

	if (src == NULL || size == 0)
		return ;
	tmp = (unsigned char *)src;
	while (size--)
		*tmp++ = '\0';
}

void	ft_putstr(const char *str)
{
	size_t	index;

	if (!str)
		return ;
	index = 0;
	while (str[index] != '\0')
		write(1, &str[index++], 1);
}

void	itoa_base_print(unsigned value, int base)
{
	char	p_buff[100];
	long	n;
	int		i;

	n = value;
	i = 1;
	while ((n /= base) >= 1)
		i++;
	p_buff[i] = '\0';
	n = value;
	while (i--)
	{
		p_buff[i] = (n % base < 10) ? n % base + '0' : n % base + 'A' - 10;
		n /= base;
	}
	if (base == 16)
		ft_putstr("0x");
	ft_putstr(p_buff);
}
