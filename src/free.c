/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <mkrutik@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/16 09:25:17 by mkrutik           #+#    #+#             */
/*   Updated: 2018/12/16 09:25:46 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/malloc.h"
#include "../include/hidden.h"

void	free(void *ptr)
{
	t_block	*block;
	t_type	type;

	block = NULL;
	pthread_mutex_lock(&g_mu);
	if (!g_info.b_lists[TINY] || !g_info.b_lists[SMALL] ||
		!g_info.b_lists[LARGE])
	{
		init();
		pthread_mutex_unlock(&g_mu);
		return ;
	}
	if (ptr == NULL || !(block = get_block_by_adress(ptr)) ||
		block->used == FALSE)
	{
		pthread_mutex_unlock(&g_mu);
		return ;
	}
	type = get_type(block->size);
	free_block(type, block);
	pthread_mutex_unlock(&g_mu);
}
